############################
#Control pecera 0.1
#30/08/18
############################


#################
#LIBRERIAS
#################

import time
import tkinter as tk
from tkinter import ttk
from tkinter import scrolledtext
from tkinter import Menu
from tkinter import messagebox as msg

#
#VARIABLES
#
temp = 50


##########
#FUNCIONES
###########

"""
Funcion dedicada a chequeo de temperatura
"""
def check_temp():
    if temp <= 30:
        tempr.configure(text="Temperatura baja: " +str(temp ))
    if temp >= 30:
        tempr.configure(text="Temperatura alta: " +str(temp ))

def check_ilu():
    encendido = 0
    if encendido == 0:
        ilur.configure(text="Apagado")
        encendido = 1
    if encendido != 1:
        ilur.configure(text="Encendido")
        encendido = 0

def _salir():
    ventana.quit()
    ventana.destroy()
    exit()

def _msgBox():
    msg.showinfo("Acerca","Version 0.1 \n 30/08/18")



###############################
#CREACION DE VENTANA
###############################

ventana = tk.Tk()

ventana.title("Control Pecera")
ventana.minsize(260, 100)
#img = PhotoImage(file='pez.ico')
#ventana.tk.call('wm', 'iconphoto' ,ventana._w, img)

#=================
#BARRA DE MENU
#================
#crea la barra de Menu
barra_menu=Menu(ventana)
ventana.config(menu=barra_menu)

#crea el menu y agrega los items
archivo_menu=Menu(barra_menu, tearoff=0)
archivo_menu.add_command(label="Salir", command= _salir)
barra_menu.add_cascade(label="Archivo", menu=archivo_menu)

ayuda_menu = Menu(barra_menu, tearoff=0)
ayuda_menu.add_command(label="Acerca", command=_msgBox)
barra_menu.add_cascade(label="Ayuda", menu=ayuda_menu)

frame_datos = ttk.LabelFrame(ventana, text="DATOS")
frame_datos.grid(column = 0, row = 0)
frame_opciones = ttk.LabelFrame(ventana, text="OPCIONES")
frame_opciones.grid(column = 0, row = 1)

###########
#ETIQUETAS
###########

temperatura = ttk.Label(frame_datos,text="Temperatura: ")
temperatura.grid(column = 0, row = 0)
iluminacion = ttk.Label(frame_datos, text="Iluminacion: ")
iluminacion.grid(column = 0, row = 3)

"""
Etiquetas que se modificaran al cambiar temperatura e iluminacion.
"""
tempr = ttk.Label(frame_datos, text="0c ")
tempr.grid(column=1, row=0)
ilur = ttk.Label(frame_datos, text="Apagado")
ilur.grid(column=1, row=3)
###############
#CAJA DE TEXTO
###############

###########
#BOTONES
###########

c = ttk.Button(frame_opciones, text="calcular", command=check_temp)
c.grid(column=0, row=4)

i = ttk.Button(frame_opciones, text="Encender", command=check_ilu)
i.grid(column=2, row=4)


#####################
#LOOP PRINCIPAL
#####################

ventana.mainloop()
